const int BUTTON_PIN = 13;
int PIEZO_PIN = 2;
bool wasClickActive = false;

void setup() {
  Serial.begin(9600);
  pinMode(BUTTON_PIN, INPUT);
}

void loop() {
  if (digitalRead(BUTTON_PIN)) {
    Serial.println("OnButtonPressed");
    if (!wasClickActive) {
      wasClickActive = true;
    }
  } else {
    if (wasClickActive) {
      Serial.println("OnButtonUp");
      generateBeeps(PIEZO_PIN, 11, 11, 200, 1000, 250, 250);
      wasClickActive = false;
    }
  }
  Serial.println("Nothing happening");
}

void generateBeeps(int piezoPin, int hourBeeps, int minuteBeeps, int hourToneFrequency, int minuteToneFrequency, int toneDuration, int betweenDelay) {
  betweenDelay = toneDuration + betweenDelay;
  for (int i = 0; i <= hourBeeps; i++) {
    tone(piezoPin, hourToneFrequency, toneDuration);
    if (hourBeeps != 1 && hourBeeps != i) {
      delay(betweenDelay);
    }
  }
  for(int i = 1; i <= minuteBeeps; i++){
    tone(piezoPin, minuteToneFrequency, toneDuration);
    if (minuteBeeps != 1 && minuteBeeps != i) {
      delay(betweenDelay);
    }
  }
}



